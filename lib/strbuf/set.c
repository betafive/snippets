/*******************************************************************************
    strbuf: set.c

    Make a string buffer from a c-style string.

    Copyright (C) 2006 Paul Barker <paul@paulbarker.me.uk>

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*******************************************************************************/

#include <assert.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include <strbuf.h>

/*
    This function copies the string to a new buffer rather than wrapping the
    existing string.
*/
int strbuf_set(strbuf_t *buf, const char *s)
{
    size_t len;

    assert(buf);
    assert(s);

    /* Free the old buffer if necessary. */
    if (buf->base)
        free(buf->base);

    /*
        TODO: This loops thru the string twice, once in strlen() and once in
        strcpy(). Think about how this can be done without looping thru
        the string more than once.
    */
    len = strlen(s);

    /*
        Round up the new limit to a multiple of STRBUF_QUANTUM. This always
        adds at least 1 to the limit, guarenteeing space for the null
        terminator.
    */
    buf->limit = len + STRBUF_QUANTUM - (len % STRBUF_QUANTUM);

    buf->base = (char *)malloc(buf->limit);

    /* Check for failure of malloc(). */
    if (! buf->base)
        return 0;

    strcpy(buf->base, s);

    buf->end = buf->base + len;

    /* Return success. */
    return 1;
}

/*******************************************************************************
    libstrbuf: valid.c

    Check if a string buffer is valid.

    Copyright (C) 2006 Paul Barker <paul@paulbarker.me.uk>

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*******************************************************************************/

#include <strbuf.h>

/* Returns 1 for a valid buffer, zero for an invalid buffer. */
int strbuf_valid(strbuf_t * buf)
{
    /*
        Check in order:
            1) Is the argument non-NULL?
            2) Is the base pointer non-NULL?
            3) Is the end pointer non-NULL?
            4) Is the end pointer within the buffer?
            5) Is the string terminated at the correct position?

        A more paranoid check would include testing that
            (strlen(buf->base) == buf->end - buf->base)
        but this is an O(n) test and so I don't like it :).

        The aim is for the tests to be O(1).
    */
    return (    buf
            &&  buf->base
            &&  buf->end
            &&  (buf->end - buf->base < buf->limit)
            &&  (*buf->end == '\0')
           )
            ? 1 : 0;
}

/*******************************************************************************
    strbuf: src/ncat.c

    Append a c-style string onto the end of a buffer, reallocating the buffer
    if necessary (size-limited version).

    Copyright (C) 2006 Paul Barker

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*******************************************************************************/

#ifdef CONFIG_HEADER
#   include CONFIG_HEADER
#endif

#include <assert.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include <strbuf.h>

/* The destination string grows by at most n characters. */
int strbuf_ncat(strbuf_t *buf, const char *s, size_t n)
{
    char * new;

    assert(buf);

#if ASSERT_STRINGS
    assert(s);
#endif

    /* Grab the length of the total string. */
    size_t len = (buf->end - buf->base) + strnlen(s, n);

    if (len >= buf->limit) {
        /*
            We need to reallocate the buffer.

            Round up the new limit to a multiple of STRBUF_QUANTUM. This
            always adds at least 1 to the limit, guarenteeing space for the
            null terminator.
        */
        buf->limit = len + STRBUF_QUANTUM - (len % STRBUF_QUANTUM);

        new = (char *)realloc(buf->base, buf->limit);

        /* Check for failure of realloc(). */
        if (!new)
            return 0;

        /*
            Update buf->end to point to the end of the reallocated string, then
            update buf->base.
        */
        buf->end = (buf->end - buf->base) + new;
        buf->base = new;
    }

    /* Now copy the new string onto the end of the old string. */
    strncpy(buf->end, s, n);

    /* Now update buf->end to the end of the resultant string. */
    buf->end = buf->base + len;

    /* Make sure the string is null terminated. */
    *(buf->end) = '\0';

    /* Return success. */
    return 1;
}

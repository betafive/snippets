/*******************************************************************************
    strbuf: cpy.c

    Copy a string from one buffer to another.

    Copyright (C) 2006 Paul Barker <paul@paulbarker.me.uk>

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*******************************************************************************/

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include <strbuf.h>

int strbuf_cpy(strbuf_t *dest, strbuf_t *src)
{
    /*
        We don't need a valid string buffer for the destination, just a non-NULL
        pointer. The source, on the other hand, does need to be valid.
    */
    assert(dest);
    assert( strbuf_valid(src) );

    /* Set members of destination structure. */
    dest->limit = src->limit;
    dest->base = (char *)malloc(dest->limit);
    dest->end = (src->end - src->base) + dest->base;

    /* Check for failure. */
    if (! dest->base)
        return 0;

    /* Copy the string. */
    strcpy(dest->base, src->base);

    return 1;
}

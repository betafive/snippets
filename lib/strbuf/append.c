/*******************************************************************************
    strbuf: append.c

    Append a character to a string buffer, reallocating the buffer if
    necessary.

    Copyright (C) 2006 Paul Barker <paul@paulbarker.me.uk>

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*******************************************************************************/

#include <assert.h>
#include <stddef.h>
#include <stdlib.h>

#include <strbuf.h>

int strbuf_append(strbuf_t *buf, int ch)
{
    char * new;

    /* Make sure we have a valid buffer. */
    assert( strbuf_valid(buf) );

    /*
        buf->end is always greater than buf->base, so we can convert it to an
        unsigned type (size_t) before doing the comparison.
    */
    if (((size_t)(buf->end - buf->base)) < (buf->limit - 1)) {
        /* Adding a new character will overflow the buffer. */

        /*
            Reallocate a larger buffer, STRBUF_QUANTUM bytes longer than the
            old buffer.

            realloc() will copy the string to the new buffer.
        */
        buf->limit += STRBUF_QUANTUM;
        new = (char *)realloc(buf->base, buf->limit);

        /* Abort operation if reallocation failed. */
        if (!new)
            return 0;

        /*
            Update buf->end to point to the end of the new string, then
            update buf->base.
        */
        buf->end = (buf->end - buf->base) + new;
        buf->base = new;
    }

    /* We can now simply tag the new character onto the end. */
    *(buf->end++) = (char) ch;
    *(buf->end) = '\0';

    /* Return success. */
    return 1;
}

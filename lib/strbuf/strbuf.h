/*******************************************************************************
    strbuf: <strbuf.h>

    Dynamic string buffers.

    Copyright (C) 2006 Paul Barker <paul@paulbarker.me.uk>

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*******************************************************************************/

#ifndef __b5_STRBUF_H_
#define __b5_STRBUF_H_

#include <stddef.h>

/*
    A string buffer, optimised for appending.

    *(buf->end) should always be '\0' for any buffer.

    Thus (buf->end - buf->base) should always be less than buf->limit.
*/
typedef struct _strbuf {
    char    *base;
    char    *end;

    size_t  limit; /* Size of buffer not size of string. */
} strbuf_t;

/* Create an empty string buffer. Returns 1 for success, 0 for failure. */
int strbuf_create(strbuf_t *buf);

/* Destroy a string buffer. Returns 1 for success, 0 for failure. */
int strbuf_destroy(strbuf_t *buf);

/*
    Reset a string buffer to contain the empty string "". Returns 1 for
    success, 0 for failure.
*/
int strbuf_clear(strbuf_t *buf);

/*
    Get a c-style string from a string buffer.

    Since the pointer returned is to the actual string stored in the buffer, any
    modifications could easily corrupt the buffer. Therefore, the returned
    string is marked as const.
*/
const char * strbuf_get(strbuf_t *buf);

/*
    Set a string buffer to a c-style string (copying the string). Returns 1 for
    success, 0 for failure.
*/
int strbuf_set(strbuf_t *buf, const char *s);

/*
    Set a string buffer to the first n characters of a c-style string (copying
    the string). Returns 1 for success, 0 for failure.
*/
int strbuf_nset(strbuf_t *buf, const char *s, size_t n);

/*
    Append a character to a string buffer. Returns 1 for success, 0 for
    failure.
*/
int strbuf_append(strbuf_t *buf, int ch);

/* Get the length of the string within a string buffer. */
size_t strbuf_len(strbuf_t *buf);

/* Get the total length of a string buffer, including padding. */
size_t strbuf_buflen(strbuf_t *buf);

/*
    Extract a c-style string from a string buffer. This function returns a
    copied string using strdup().

    The return value should be passed to free when you have finished with it.
*/
char * strbuf_extract(strbuf_t *buf);

/*
    Copy a string buffer, overwriting whatever is in the destination buffer.
     Returns 1 for success, 0 for failure.

    This will free the destination buffer (not the strbuf_t object) if it is
    non-NULL. So do not use this function with an uninitialized string buffer!
    If in doubt, use strbuf_zap first.
*/
int strbuf_cpy(strbuf_t *dest, strbuf_t *src);

/*
    Append a c-style string to a string buffer. Returns 1 for success, 0 for
    failure.
*/
int strbuf_cat(strbuf_t *buf, const char *s);

/*
    Append the first n characters of a c-style string to a string buffer.
    Returns 1 for success, 0 for failure.
*/
int strbuf_ncat(strbuf_t *buf, const char *s, size_t n);

/*
    Forcibly invalidate a string buffer without releasing memory. Useful if you
    do not know if a buffer contains valid data.

    Always succeeds if buf is non-NULL.
*/
void strbuf_zap(strbuf_t *buf);

/*
    Check if a string buffer is valid. Returns 1 for a valid buffer, 0 for
    invalid. The intended use is:
        assert( strbuf_valid(buf) );
*/
int strbuf_valid(strbuf_t *buf);

#endif /* !__b5_STRBUF_H_ */

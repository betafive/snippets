/*******************************************************************************
    divdi3.c

    generic __divdi3() implementation.

    Copyright (C) 2006 Paul Barker <paul@paulbarker.me.uk>

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*******************************************************************************/

#include <stddef.h>
#include <stdlib.h>
#include <llmath.h>

/* Long long division. */
ll_t __divdi3(ll_t numer, ll_t denom)
{
    int sign = 0;   /* 0 for pos, 1 for neg. */

    if (((numer >= 0) && (denom < 0)) || ((numer < 0) && (denom >= 0)))
        sign = 1;

    numer = llabs(numer);
    denom = llabs(denom);

    ll_t ret = (ll_t)_ulldivmod((ull_t)numer, (ull_t)denom, NULL);

    if (sign)
        ret = -ret;

    return ret;
}

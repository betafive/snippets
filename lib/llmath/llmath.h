/*******************************************************************************
    <llmath.h>

    Long long math library.

    Copyright (C) 2006 Paul Barker <paul@paulbarker.me.uk>

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*******************************************************************************/

#ifndef __b5_LLMATH_H_
#define __b5_LLMATH_H_

#include <limits.h>

/* Long long types. */
typedef long long		ll_t;
typedef unsigned long long	ull_t;

#define LONGLONG_BITS		(CHAR_BIT * sizeof(ll_t))

/*
    Divide numer by denom, returning the quotient storing the remainder in rem.
*/
ll_t _lldivmod(ll_t numer, ll_t denom, ll_t * rem);
ull_t _ulldivmod(ull_t numer, ull_t denom, ull_t * rem);

/* Functions implicitly called for operations on a long long. */
ll_t __divdi3(ll_t numer, ll_t denom);
ll_t __moddi3(ll_t numer, ll_t denom);
ull_t __udivdi3(ull_t numer, ull_t denom);
ull_t __umoddi3(ull_t numer, ull_t denom);

#endif /* !__b5_LLMATH_H_ */

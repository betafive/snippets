/*******************************************************************************
    ulldivmod_hw.c

    _ulldivmod() implementation for platform where all operations except divide
    and modulus are supported in hardware.

    Copyright (C) 2006 Paul Barker <paul@paulbarker.me.uk>

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*******************************************************************************/

#include <limits.h>
#include <llmath.h>

/*
    Divide numer by denom, storing the remainder in rem.

    I hope the explaination of the algorith makes sense. At the end of the
    calculation, the remainder is stored in numer.

    At all points in the function the following should hold true:
        denom = (orginal denom) << pos
    except, obviously, the points at which denom and pos are being updated.
*/
ull_t _ulldivmod(ull_t numer, ull_t denom, ull_t * rem)
{
    ull_t res = 0;
    int pos = 0;

    /* Special case 1: denom > numer. */
    if (denom > numer) {
        goto done;
    }

    /* Special case 2: denom <= 1. */
    if (denom <= 1) {
        /* Special case 2a: denom = 1. */
        if (denom) {
            res = numer;
            numer = 0;  /* Dividing by 1 always leaves a remainder of 0. */
        }

        /* Special case 2b: denom = 0. */
        else {
            /* Force a divide by zero error. */
            volatile int one = 1;
            volatile int zero = 0;

            one = one / zero;
        }
        goto done;
    }

    /* Special case 3: both numer and denom fit into an int. */
    if ((numer <= UINT_MAX) && (denom <= UINT_MAX)) {
        res = (ull_t) (((uint)numer) / ((uint)denom));
        numer = (ull_t) (((uint)numer) % ((uint)denom));
        goto done;
    }

    /* Special case 4: numer = denom. */
    if (numer == denom) {
        res = 1;
        numer = 0;
        goto done;
    }

    /*
        General case, (numer > denom) and (denom > 1) and long long division is
        needed.

        Firstly, left shift denom as far as possible without making it greater
        than numer. Increment pos each time so we know how far we can shift
        back.
    */
    while (numer > denom) {
        /* Test the top bit of denom. */
        if (denom & (1ULL << (LONGLONG_BITS - 1)))
            /*
                We can't go any further but numer is still greater than denom,
                so finish and skip the adjustment after this loop.
            */
            goto mainloop;

        denom <<= 1;
        pos++;
    }

    /* numer is now greater than denom, so we have gone 1 shift too far. */
    denom >>= 1;
    pos--;

    /*
        Now we do binary long division.

        We loop thru testing how many times denom goes into numer at each step,
        but since we are in binary this can only be 0 or 1 times. This makes
        testing simple, if (numer > denom) then denom goes into numer once,
        otherwise denom does not go into numer.

        If denom does go into numer, we reduce numer by denom to find the
        remainder for this step. We also store a 1 in the appropriate bit of
        the result.

        After each step we right shift denom since at this point numer is less
        than denom. To make sure we do not right shift too far, we use pos as
        a counter of how many steps we have remaining.

        When we finish, numer contains the remainder of the last step which is
        the remainder of the whole division.
    */

mainloop:
    while (pos >= 0) {
        if (numer >= denom) {
            numer -= denom;

            /*
                The previous steps have the effect of:
                    denom = (orginal denom) << pos
                so the appropriate bit to set in the result is given by pos.
            */
            res |= (1ULL << pos);
        }

        denom >>= 1;
        pos--;
    }

done:
    if (rem)
        *rem = numer;

    return res;
}

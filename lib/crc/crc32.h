/*******************************************************************************
    <crc32.h>

    CRC32 functions.

    This code is public domain.
*******************************************************************************/

#ifndef __b5_CRC32_H_
#define __b5_CRC32_H_

#include <stdint.h>
#include <stddef.h>

uint32_t crc32_le(uint32_t crc, char const *buf, size_t len);

#endif /* !__b5_CRC32_H_ */

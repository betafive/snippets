/*******************************************************************************
    pfmt_s.c

    Formatted output of a string.

    Copyright (C) 2006 Paul Barker <paul@paulbarker.me.uk>

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*******************************************************************************/

#include <pfmt.h>
#include <string.h>

#include "pfmt_internal.h"

int pfmt_s(pfmt_fn fn, void* arg, const char * s, int width, int max_len)
{
    int r = 0;
    int count = 0;

    if (width > 0) {
        /*
            We must right-align the string, without allocating a temporary
            buffer.
        */
        size_t len;

        if (max_len >= 0)
            /* Limit length measurement. */
            len = strnlen(s, max_len);
        else
            len = strlen(s);

        size_t pad = (size_t)width;

        /* Print padding is needed. */
        while (len++ < pad)
            OUTPUT(' ');
    }

    if (max_len >= 0) {
        /* Limit output to the given length. */
        while (max_len-- && *s) {
            OUTPUT(*s);
            s++;
        }
    }
    else {
        /* Simple output. */
        while (*s) {
            OUTPUT(*s);
            s++;
        }
    }

    if (width < 0) {
        width = -width;

        while (count < width)
            OUTPUT(' ');    /* Increments count. */
    }

    return r;
}

/* NOTE: Keep precision (max_len) handling upto date with pfmt_parse */

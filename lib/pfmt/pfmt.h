/*******************************************************************************
    <pfmt.h>

    Text formatting.

    Copyright (C) 2006 Paul Barker <paul@paulbarker.me.uk>

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*******************************************************************************/

#ifndef __b5_PFMT_H_
#define __b5_PFMT_H_

#include <stdarg.h>
#include <stddef.h>

/*
    Function called to output text.

    int out (int ch, void *arg);

    The function should output ch in whatever way is desired. The extra
    argument is exactly as passed to pfmt().

    Return >=0 to continue, <0 to stop. You should return the number of
    characters printed after any additional conversion steps.
*/
typedef int (*pfmt_fn)(int, void *);

/*
        Output formatted text using fn().

        Returns the sum of the return values from fn(), if they are all positive or
        zero. If one is negative (thus stopping output), this value is returned.
*/
int pfmt (pfmt_fn fn, void *arg, const char *fmt, ...);
int vpfmt (pfmt_fn fn, void *arg, const char *fmt, va_list va);

int pfmt_parse (pfmt_fn fn,
                void * arg,
                const char *fmt,
                va_list *vap,
                const char **end);

int pfmt_int (pfmt_fn       fn,
              void         *arg,
              int           base,
              int           flags,
              int           width,
              int           min_digits,   /* Precision. */
              int           sign,
              uintmax_t     value);

int pfmt_c(pfmt_fn fn, void* arg, int ch, int width);

int pfmt_s(pfmt_fn fn, void* arg, const char * s, int width, int max_len);

#define PFMT_PRINT_POSITIVE     0x01
#define PFMT_SIGN_PADDING       0x02
#define PFMT_HASH               0x04
#define PFMT_PAD_ZEROS          0x08
#define PFMT_UPPER_CASE         0x10
#define PFMT_PRINT_NULL         0x20

#endif /* !__b5_PFMT_H_ */

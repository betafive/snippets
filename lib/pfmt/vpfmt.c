/*******************************************************************************
    vpfmt.c

    vpfmt() function.

    Copyright (C) 2006 Paul Barker <paul@paulbarker.me.uk>

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*******************************************************************************/

#include <assert.h>
#include <stdarg.h>

#include <pfmt.h>

#include "pfmt_internal.h"

/* TODO: Move these to the appropriate places. */
#define PFMT_POINTER_DIGITS     8
#define PFMT_POINTER_TYPE       'l'

#define HANDLE_STYPE(nam, typ, arg_typ)             \
    case nam:                                       \
    {                                               \
        typ __t = (typ)va_arg(*vap, arg_typ);       \
                                                    \
        if (__t < 0) {                              \
            __t = -__t;                             \
            sign = '-';                             \
        }                                           \
                                                    \
        value = (uintmax_t)__t;                     \
        break;                                      \
    }

#define HANDLE_UTYPE(nam, typ, arg_typ)             \
    case nam:                                       \
        value = (uintmax_t)va_arg(*vap, arg_typ);   \
        break;

static int pfmt_parse_int (pfmt_fn     fn,
                           void       *arg,
                           int         base,
                           int         flags,
                           int         width,
                           int         min_digits,   /* Precision. */
                           int         signed_type,
                           int         type,
                           va_list    *vap)
{
/*  assert(fn);
    assert(base >= 2);
    assert(base <= 36); */

    int r = 0;
    uintmax_t value;

    /*
        Type-specific handlers...

        nr_digits is set to zero if the number is zero, rather than nr_digits
        being 1 and that digit being '0'.
    */
    if (signed_type) {
        switch (type) {

            HANDLE_STYPE('H', schar, int);
            HANDLE_STYPE('h', short, int);
            HANDLE_STYPE('l', long, long);
            HANDLE_STYPE('L', ll_t, ll_t);
            HANDLE_STYPE('j', intmax_t, intmax_t);
/*            HANDLE_STYPE('z', ssize_t, ssize_t);          */
            HANDLE_STYPE('t', ptrdiff_t, ptrdiff_t);
            HANDLE_STYPE(0, int, int);

            default:
/*              assert( 0 && "Invalid type argument to pfmt_int"); */
                return -1;
        }
    }
    else {
        switch (type) {

            HANDLE_UTYPE('H', uchar, int);
            HANDLE_UTYPE('h', ushort, int);
            HANDLE_UTYPE('l', ulong, ulong);
            HANDLE_UTYPE('L', ull_t, ull_t);
            HANDLE_UTYPE('j', uintmax_t, uintmax_t);
            HANDLE_UTYPE('z', size_t, size_t);
/*            HANDLE_UTYPE('t', uptrdiff_t, uptrdiff_t);    */
            HANDLE_UTYPE(0, uint, uint);

            default:
/*              assert( 0 && "Invalid type argument to pfmt_int"); */
                return -1;
        }
    }

    DISPATCH( pfmt_int(fn, arg, base, flags, width, min_digits, sign, value) );

    return r;
}

/* Simple atoi & advance the given pointer past the number read. */
static inline int _atoi_advance(const char **s)
{
    int x = 0;

    while (isdigit( **s ))
        x = (x * 10) + (*(*s)++ - '0');

    return x;
}

/* Helper to print a pointer argument. */
static int _ptr_helper(pfmt_fn fn,
                       void * arg,
                       int width,
                       ...)
{
    va_list va;

    va_start(va, width);

    int ret = pfmt_parse_int(fn, arg, 16, PFMT_HASH, width, PFMT_POINTER_DIGITS,
                             0, PFMT_POINTER_TYPE, &va);

    va_end(va);

    return ret;
}

/* Sets *end to NULL if the conversion fails. */
static int pfmt_parse (pfmt_fn          fn,
                       void            *arg,
                       const char      *fmt,
                       va_list         *vap,
                       const char     **end)
{
/*  assert(fn); */

    int r = 0;
    int flags = 0;

    int width;
    int length;
    int precision;

    int spec;

    /* Alignment: 0 for right, 1 for left. */
    int align = 0;

    /***************************************************************************
        Flags.
    ***************************************************************************/
    while (1) {
        switch (*fmt) {
            case '%':   /* Output '%'. */
                OUTPUT(*fmt);

                if (end)
                    *end = ++fmt;
                return r;
            case '+':   /* Print a '+' infront of +ve numbers. */
                flags |= PFMT_PRINT_POSITIVE;
                break;
            case ' ': /* Print a ' ' infront of +ve numbers. */
                flags |= PFMT_SIGN_PADDING;
                break;
            case '-': /* Left-align within the field. */
                align = 1;
                break;
            case '0': /* Pad a number with zeroes rather than spaces. */
                flags |= PFMT_PAD_ZEROS;
                break;
            case '#': /* Eh, work it out yourself. RTFM. */
                flags |= PFMT_HASH;
                break;
            default:
                goto after_flags;
        }

        fmt++;
    }

after_flags:

    /***************************************************************************
        Width.
    ***************************************************************************/
    if (*fmt == '*')
        width = va_arg(*vap, int);
    else if ( isdigit(*fmt) )
        width = _atoi_advance(&fmt);
    else
        width = 0;

    /* Fix the width if a '-' was given during flags. */
    if (align)
        width = -width;

    /***************************************************************************
        Precision.
    ***************************************************************************/

    /* Precision starts with a '.'. */
    if (*fmt == '.') {
        fmt++;

        if (*fmt == '*')
            precision = va_arg(*vap, int);
        else if ( isdigit(*fmt) )
            precision = _atoi_advance(&fmt);
        else
            /*
                A '.' has been given but no number, the standard says we set
                the precision to 1.
            */
            precision = 1;
    }
    else
        /*
            Default precision, which disables the precision handling of pfmt_s
            & pfmt_int. NOTE: This value is not 0! 0 has a different meaning for
            pfmt_s.

            Changes in precision handling in either of these functions should be
            reflected here. Also watch the semantics when adding functions to
            handle different conversion specifiers.

            NOTE: Keep an eye on this.
        */
        precision = -1;

    /***************************************************************************
       Length modifier.
    ***************************************************************************/
    switch (*fmt) {
        case 'h':
            if (*(++fmt) == 'h')
                length = 'H';
            else
                length = 'h';
            break;
        case 'l':
            if (*(++fmt) == 'l')
                length = 'L';
            else
                length = 'l';
            break;
        case 'j':
        case 'z':
        case 't':
        case 'L':
            length = *fmt;
            ++fmt;
            break;
        default:
            length = 0;
    }

    /***************************************************************************
        Specifier, or abort on *fmt == '\0'.
    ***************************************************************************/
    if (*fmt)
        spec = *fmt++;
    else {
        /* Report failure. */
        fmt = NULL;
        goto done;
    }

    /***************************************************************************
        Determine appropriate conversion and possibly dispatch.
    ***************************************************************************/

    int signed_type = 0;

    switch (spec) {
        case 'd':
        case 'i':
            signed_type = 1;
        case 'u':
            DISPATCH( pfmt_parse_int(fn, arg, 10, flags, width, precision,
                                     signed_type, length, vap) );
            break;

        case 'o':
            DISPATCH( pfmt_parse_int(fn, arg, 8, flags, width, precision, 0,
                                     length, vap) );
            break;

        case 'X':
            flags |= PFMT_UPPER_CASE;
        case 'x':
            DISPATCH( pfmt_parse_int(fn, arg, 16, flags, width, precision, 0,
                                     length, vap) );
            break;

        case 'c':
            DISPATCH( pfmt_c(fn, arg, va_arg(*vap, int), width) );
            break;

        case 's':
            DISPATCH( pfmt_s(fn, arg, va_arg(*vap, const char *), width,
                             precision) );
            break;

        case 'p':
            /*
                Treat "%p" as "%#.?x" where ? is set to the number of hex digits
                in a pointer value on this platform, except "(NULL)" is printed
                for null pointers. The printing of "(NULL)" is handled by
                pfmt_int when the PFMT_PRINT_NULL flag is given.

                We leave the width as is but ignore the given precision and
                flags.

                The number of digits in a pointer (PFMT_POINTER_DIGITS) and the
                length modifier to use (PFMT_POINTER_TYPE) are determined by the
                current architecture. See the appropriate config file
                (eg. arch/x86/config) and the "mkheaders.sh" script.
            */
        {
            uintptr_t p = (uintptr_t)va_arg(*vap, void *);

            if (p)
                DISPATCH( _ptr_helper(fn, arg, width, p) );
            else
                DISPATCH( pfmt_s(fn, arg, "(NULL)", width, -1) );

            break;
        }

        case 'n':

        case 'f':
        case 'F':
        case 'e':
        case 'E':
        case 'g':
        case 'G':
        case 'a':
        case 'A':
            /* TODO. */
            /* For now, fall thru to default handler. */

        default:
            /* Failed to handle a character, report failure. */
            fmt = NULL;
    }

    /***************************************************************************
        All done.
    ***************************************************************************/
done:
    if (end)
        *end = fmt;
    return r;
}

int vpfmt (pfmt_fn fn, void *arg, const char *fmt, va_list va)
{
/*  assert(fn); */

    int r = 0;
    const char * end = NULL;

    /* Main loop. */
    while (*fmt) {
        if ( !(*fmt == '%') ) {
            OUTPUT(*fmt);
            fmt++;
        }
        else {
            fmt++;

            /* Now enter parser. */
            int tmp = pfmt_parse(fn, arg, fmt, (va_list *)&va, &end);

            if (tmp >= 0)
                r += tmp;
            else
                return tmp;

            /*
                If end is non-NULL, the conversion worked so advance fmt past
                the conversion specifier, else the conversion failed so print
                the unconverted text.
            */
            if (end)
                fmt = end;
            else
                OUTPUT('%');

        }
    }

    /* All done. */
    return r;
}

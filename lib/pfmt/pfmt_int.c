/*******************************************************************************
    pfmt_int.c

    Formatted output of an integer.

    Copyright (C) 2006,2007 Paul Barker <paul@paulbarker.me.uk>

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*******************************************************************************/

#include <assert.h>
#include <ctype.h>
#include <pfmt.h>
#include <stddef.h>
#include <stdint.h>
#include <utypes.h>

#include "pfmt_internal.h"

/* Convert a single digit. Assumes d < 36. */
#define MAKE_DIGIT(d) ((d < 10) ? d + '0' : d + 'a' - 10)

/*
    2^10 ~= 10^3, so for every 10 bits in an integer we have just over 3
    decimal digits. So allowing 40 digits is enough to support integers upto
    around 128 bits, without being too excessive.
*/
#define DIGITS      40

int pfmt_int (pfmt_fn       fn,
              void         *arg,
              int           base,
              int           flags,
              int           width,
              int           min_digits,   /* Precision. */
              int           sign,
              uintmax_t     value)
{
    int r = 0;
    const char *prefix = NULL;

    /* Digit stack. */
    char buf[DIGITS];
    int nr_digits = 0;

    /* Length of number to be printed, including extras. */
    int len = 0;

    /* Amount of inside padding (caused by min_digits). */
    int padding = 0;

    while (value) {
        int d = value % base;

        buf[nr_digits++] = MAKE_DIGIT(d);
        len++;

        value /= base;
    }

    if (!sign) {
        if (flags & PFMT_PRINT_POSITIVE)
            sign = '+';
        else if (flags & PFMT_SIGN_PADDING)
            sign = ' ';
    }

    if (sign != 0)
        len++;

    /* Determine prefix. */
    if (flags & PFMT_HASH) {
        if ( (base == 16) && nr_digits ) {
            /* Prefix non-zero hex numbers with 0x. */
            prefix = "0x";
            len += 2;
        }
        else if ( (base == 8) && nr_digits ) {
            /* Prefix non-zero octal numbers with 0. */
            prefix = "0";
            len += 2;
        }
    }

    /*
        Since nr_digits is incremented once per digit to be printed and starts at
        zero, it gives us the actual number of digits without any padding (which
        is included in len.
    */
    if (nr_digits < min_digits) {
        /* Record the amount of padding we will add later. */
        padding = min_digits - nr_digits;
        len += padding;
    }

    /**************************************************************************
        Now we have a number and we are ready for output!
    **************************************************************************/
    /*
        Deal with padding to the left of the number.

        Outside padding with spaces goes before the sign and prefix.
    */
    if ( (width > 0) && !(flags & PFMT_PAD_ZEROS) ) {
        while (len++ < width)
            OUTPUT(' ');
    }

    /* Output the sign then the prefix. */
    if (sign)
        OUTPUT(sign);

    if (prefix) {
        while (*prefix) {
            OUTPUT(*prefix);
            prefix++;
        }
    }

    /*
        Deal with padding to the left of the number.

        Outside padding with zeros goes after the sign and prefix.
    */
    if ( (width > 0) && (flags & PFMT_PAD_ZEROS) ) {
        while (len++ < width)
            OUTPUT('0');
    }

    if ( (!padding) && (!nr_digits) )
    {
        /* Make sure at least one digit is printed. */
        OUTPUT('0');
    }
    else {
        /* Ouput the inside padding then the number. */
        while (padding--)
            OUTPUT('0');

        if (nr_digits) {
            while (nr_digits--) {
                char ch = buf[nr_digits];

                if (flags & PFMT_UPPER_CASE)
                    OUTPUT(toupper(ch));
                else
                    OUTPUT(ch);
            }
        }
    }

    /* Deal with padding to the right of the number. */
    if (width < 0) {
        width = -width;

        while (len++ < width)
            OUTPUT(' ');
    }

    return r;
}

/* NOTE: Keep precision (min_digits) handling upto date with pfmt_parse */

/*******************************************************************************
	log.h: Generic logging support.

	Copyright (C) 2013 Paul Barker

	Permission is hereby granted, free of charge, to any person obtaining a
	copy of this software and associated documentation files (the
	"Software"), to deal in the Software without restriction, including
	without limitation the rights to use, copy, modify, merge, publish,
	distribute, sublicense, and/or sell copies of the Software, and to
	permit persons to whom the Software is furnished to do so, subject to
	the following conditions:

	The above copyright notice and this permission notice shall be included
	in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
	MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
	IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
	CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
	TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

#ifndef __LOG_H_INCLUDED__
#define __LOG_H_INCLUDED__

#include <sys/cdefs.h>

#ifndef __noreturn
#define __noreturn __attribute__ ((noreturn))
#endif

/**
 * Initilize logging subsystem.
 *
 * \param output_file Path and filename to write log messages to. This file will
 * be created if it doesn't exist, otherwise the output will be appended to the
 * file.
 *
 * \param app_name Name for the current application. Enables the source of log
 * messages to be determined if multiple programs write to the same log file.
 *
 * \returns 0 on success, <0 on failure.
 */
int log_init(const char *output_file, const char *app_name);

/**
 * Shutdown the logging subsystem.
 */
void log_exit();

/**
 * Print output to a log file.
 *
 * \param level Severity level. If this is LOG_FATAL, the call aborts the
 * application after the log message is written.
 *
 * \param s printf-style format string, followed by arguments.
 */
int log_printf(int level, const char *s, ...);

/**
 * Equivalent to log_printf(LOG_MESSAGE, s, ...).
 */
int msg(const char *s, ...);

/**
 * Equivalent to log_printf(LOG_WARNING, s, ...).
 */
int warn(const char *s, ...);

/**
 * Equivalent to log_printf(LOG_ERROR, s, ...).
 */
int error(const char *s, ...);

/**
 * Equivalent to log_printf(LOG_FATAL, s, ...).
 *
 * This function terminates the application and so does not return.
 */
void __noreturn fatal(const char *s, ...);

/**
 * Sync the log output to disk.
 */
void log_sync();

/**
 * Valid log levels in decreasing order of severity.
 */
enum __log_levels {
	LOG_FATAL,
	LOG_ERROR,
	LOG_WARNING,
	LOG_MESSAGE,
	LOG_MAX_LEVEL		/* Not valid for use. */
};

#endif /* !__LOG_H_INCLUDED__ */

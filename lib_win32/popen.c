/*******************************************************************************
    popen.c

    Emulation of popen for win32.

    Copyright (C) 2007 Paul Barker <paul@paulbarker.me.uk>

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*******************************************************************************/

#include <windows.h>
#include <io.h>

FILE *
popen(const char *command, const char *type)
{
    HANDLE hRead, hWrite;
    BOOL b;
    STARTUP_INFO si;
    FILE *f;

    b = CreatePipe(&hRead, &hWrite, NULL, 0);
    if (!b)
        return NULL;

    memset(&si, 0, sizeof(si));
    si.cb = sizeof(si);
    si.hStandardOutput = hWrite;

    b = CreateProcess(NULL, command, NULL, NULL, TRUE, 0, NULL, NULL, &si,
                      NULL);

    f = _open_osfhandle(hRead, 0);
    return f;
}
